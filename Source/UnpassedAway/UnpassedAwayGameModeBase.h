// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnpassedAwayGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNPASSEDAWAY_API AUnpassedAwayGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
